#[derive(Debug, Clone, Copy)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}
impl Color {
    pub fn mix(&self, other: &Color, factor: f64) -> Color {
        let mix_byte = |a, b| ((1.0 - factor) * f64::from(a) + factor * f64::from(b)) as u8;
        Color {
            r: mix_byte(self.r, other.r),
            g: mix_byte(self.g, other.g),
            b: mix_byte(self.b, other.b),
        }
    }
}

pub const BLACK: Color = Color { r: 0, g: 0, b: 0 };

pub const WHITE: Color = Color {
    r: 255,
    g: 255,
    b: 255,
};

pub const CINNABAR: Color = Color {
    r: 0xE3,
    g: 0x42,
    b: 0x34,
};

pub const GREEN: Color = Color {
    r: 25,
    g: 75,
    b: 25,
};

pub const CYAN: Color = Color {
    r: 0x88,
    g: 0xc0,
    b: 0xf0,
};
