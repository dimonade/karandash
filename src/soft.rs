use crate::colors::Color;

pub mod prelude {
    pub use super::{sdf::*, Drawer};
}

pub mod sdf {

    #[inline]
    pub fn sd_circle(
        p: (f64, f64),
        center: (f64, f64),
        outer_radius: f64,
        inner_radius: f64,
    ) -> f64 {
        sd_intersect(
            sd_disk(p, center, outer_radius),
            -sd_disk(p, center, inner_radius),
        )
    }

    #[inline]
    pub fn sd_disk(point: (f64, f64), center: (f64, f64), radius: f64) -> f64 {
        let x = point.0 - center.0;
        let y = point.1 - center.1;
        (x * x + y * y).sqrt() - radius
    }

    #[inline]
    pub fn sd_union(a: f64, b: f64) -> f64 {
        a.min(b)
    }

    #[inline]
    pub fn sd_intersect(a: f64, b: f64) -> f64 {
        a.max(b)
    }
}

pub struct Drawer {
    width: u32,
    height: u32,
    default_color: Color,
    lower: f64,
    higher: f64,
    fill: Vec<Filler>,
}

struct Filler {
    color: Color,
    sdf: Box<dyn Fn((f64, f64)) -> f64>,
}

impl Drawer {
    pub fn new(width: u32, height: u32, default_color: Color) -> Self {
        Self {
            width,
            height,
            default_color,
            lower: -0.5,
            higher: 0.5,
            fill: vec![],
        }
    }

    pub fn smoothing(mut self, lower: f64, higher: f64) -> Self {
        self.lower = lower;
        self.higher = higher;
        self
    }

    pub fn fill<F>(&mut self, color: Color, sdf: F) -> &mut Self
    where
        F: Fn((f64, f64)) -> f64 + 'static,
    {
        self.fill.push(Filler {
            color,
            sdf: Box::new(sdf),
        });
        self
    }

    pub fn to_vec_rgb8(&self) -> Vec<u8> {
        let num_pixels = self.width * self.height;
        (0..num_pixels)
            .map(|xy| {
                let p = (xy % self.width, self.width - xy / self.width);
                let p = (p.0.into(), p.1.into());
                let mut color = self.default_color;
                for fill in &self.fill {
                    let d = (fill.sdf)(p);
                    if d < self.lower {
                        color = fill.color;
                        break;
                    } else if d < self.higher {
                        color = fill
                            .color
                            .mix(&color, (d - self.lower) / (self.higher - self.lower));
                    }
                }
                color
            })
            .flat_map(|Color { r, g, b }| [r, g, b].into_iter())
            .collect()
    }
}
