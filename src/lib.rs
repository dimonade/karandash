//! Karandash, russian for pencil, is yet another plotting library
//!
//! Its building block is the signed distance field, which is a way to divide
//! the image into inside and outside, and allows for smooth edges for any
//! image size.

/// Built-in colors to get you started
pub mod colors;

/// Software based rendering of SDF to figures
pub mod soft;
