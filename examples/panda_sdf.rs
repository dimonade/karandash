use karandash::{colors, soft::prelude::*};

use std::f64::consts::SQRT_2;

const OUT_FILE: &str = concat!(env!("CARGO_MANIFEST_DIR"), "/target/panda-sdf.png");
const WIDTH: u32 = 100;
const HEIGHT: u32 = 100;

const SDF_SIZE: f64 = 32.0;
const OUTLINE_WIDTH: f64 = 5.0;

const EAR_OFFSET: f64 = 1.35 * SDF_SIZE / SQRT_2;
const EYE_OFFSET: f64 = 0.5 * SDF_SIZE / SQRT_2;
const EYE_SIZE: f64 = 0.3 * SDF_SIZE;
const EYE_INNER_SIZE: f64 = EYE_SIZE - 1.4 * OUTLINE_WIDTH;
const FACE_OFFSET: f64 = 2.5;

fn main() {
    image::save_buffer(
        OUT_FILE,
        &draw(WIDTH, HEIGHT),
        WIDTH,
        HEIGHT,
        image::ColorType::Rgb8,
    )
    .unwrap();
}

fn draw(width: u32, height: u32) -> Vec<u8> {
    let start = std::time::Instant::now();
    let pixels = Drawer::new(width, height, colors::WHITE)
        .fill(colors::BLACK, sdf_panda)
        .to_vec_rgb8();

    println!(
        "drawer finished in {} seconds",
        start.elapsed().as_secs_f64()
    );
    pixels
}

fn sdf_panda(p: (f64, f64)) -> f64 {
    let p = (p.0, p.1);
    let p = (p.0 - f64::from(WIDTH / 2), p.1 - f64::from(HEIGHT / 2));

    let head = sd_circle(p, Default::default(), SDF_SIZE, SDF_SIZE - OUTLINE_WIDTH);

    let ears = sd_union(
        sd_disk(p, (-EAR_OFFSET, EAR_OFFSET), 0.5 * SDF_SIZE),
        sd_disk(p, (EAR_OFFSET, EAR_OFFSET), 0.5 * SDF_SIZE),
    );

    // move the eyes and nose down
    let p = (p.0, p.1 + FACE_OFFSET);

    let eyes = sd_union(
        sd_circle(p, (-EYE_OFFSET, EYE_OFFSET), EYE_SIZE, EYE_INNER_SIZE),
        sd_circle(p, (EYE_OFFSET, EYE_OFFSET), EYE_SIZE, EYE_INNER_SIZE),
    );

    let nose = sd_disk(p, Default::default(), SDF_SIZE / 10.0);

    sd_union(sd_union(head, ears), sd_union(eyes, nose))
}
